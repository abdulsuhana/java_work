package carinventoryproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Scanner;


public class Car
{
	static Connection con=null;
	public static void main(String[] args) throws SQLException 
	{
		Statement st=null;

		try 
		{
			con=DBConnection.getConnection();
			st = con.createStatement();
		   // st.execute("create table cars(id varchar(30) primary key,make varchar(30),model varchar(30),year int,sales_price varchar(30))");
			Scanner sc = new Scanner(System.in);
			System.out.println("Welcome to Mullet Joe's Gently Used Autos!");
			System.out.println("The commands are ");
			System.out.println("1.add");
			System.out.println("2.list");
			System.out.println("3.update");
			System.out.println("4.delete ");
			System.out.println("5.quit");
			while(true) {
				System.out.print("Enter command : ");
				String command = sc.nextLine();
				if(command.equals("quit")) 
				{
					break;
				}
				else 
				{
					ResultSet rs= st.executeQuery("select count(*) from cars");
					rs.next();
					int count = rs.getInt(1);
					commands(command,count);
				}
			}
			System.out.println("Good Bye!");


		}
		catch (Exception e) 
		{
			System.out.println(e);
		}
		//		  finally
		//		  {
		//			  st.close();
		//			  con.close();
		//		  }

	}
           public static void commands(String command,int count) throws SQLException 
	{
		Statement st = con.createStatement();
		switch(command)
		{
		case "add":
			try
			{
				if(count<20) 
				{
					Scanner sc = new Scanner(System.in);
					System.out.println("Make : ");
					String make = sc.nextLine();
					System.out.println("Model : ");
					String model = sc.nextLine();
					System.out.println("Year : ");
					int year = sc.nextInt();
					System.out.println("Sales Price ($) :");
					float sales_price = sc.nextFloat();
					Car_Details cd  = new Car_Details(make,model,year,sales_price);
					System.out.println("Enter unique id:");
					int id = sc.nextInt();
					String query1 = "insert into cars (id, make, model,year,sales_price)" + "values("+id+",'"+cd.getMake()+"','"+cd.getModel()+"',"+cd.getYear()+","+cd.getSales_price()+")";
					System.out.println(query1);
					int noOfRows=st.executeUpdate(query1);
					System.out.println(noOfRows+" inserted!! ");
				}
				else 
				{
					System.out.println("Limit Reached!!");
				}

			}
			catch(InputMismatchException e) 
			{
				e.printStackTrace();
			}

			break;
			
		case "list":
			if(count==0) 
			{
				System.out.println("There are currently no cars in the catalog.");
			}
			else 
			{
				int total = 0;
				ResultSet rs = st.executeQuery("select * from cars");
				while(rs.next()) 
				{
					int id=rs.getInt("id");
					String make = rs.getString("make");
					String model = rs.getString("model");
					int year = Integer.parseInt(rs.getString("year"));
					int sales = (int)(rs.getFloat("sales_price"));
					total+=sales;
					System.out.println(id+" "+year+"  "+make+"  "+model+"  $"+sales);
				}
				System.out.println("Number of cars "+count);
				System.out.println("Total inventory :  $"+total);


			}
			break;

		case "delete":
			Scanner sc = new Scanner(System.in);
			System.out.println("enter id to delete from the list");
			int delete_id=sc.nextInt();
			st.executeUpdate("delete from cars where id="+delete_id);
			System.out.println("Record deleted successfully");
			break;

		case "update":
			Scanner sc1 = new Scanner(System.in);
			System.out.println("Enter the ID of the car you want to update");
			String id_update=sc1.nextLine();
			System.out.println("Enter the field you want to update");
			String model_select=sc1.nextLine();
			switch(model_select) 
			{
			   case "model":
				   try
				   {
					   System.out.println("Enter the new model of the car to update");
					   String new_model=sc1.nextLine();
					   int noOfRowsUpdated=st.executeUpdate("update cars set model='"+new_model+"' where id='"+id_update+"'");
					   System.out.println(noOfRowsUpdated+" rows updated");
				   }
				   catch(Exception e) {
					   e.printStackTrace();
				   }
				break;
			case "year":
				System.out.println("Enter the new year");
				int year=Integer.parseInt(sc1.nextLine());
				int noOfRowsUpdated=st.executeUpdate("update cars set year='"+year+"' where id='"+id_update+"'");
				System.out.println(noOfRowsUpdated+" rows updated");
				break;
			case "make":
				System.out.println("Enter the new make");
				String new_make=sc1.nextLine();
				int noOfRowsUpdated1=st.executeUpdate("update cars set make='"+new_make+"' where id='"+id_update+"'");
				System.out.println(noOfRowsUpdated1+" rows updated");
				break;
			case "price":
				try
				{
					System.out.println("Enter the new price");
					double price=Double.parseDouble(sc1.nextLine());
					int noOfRowsUpdated2=st.executeUpdate("update cars set sales_price='"+price+"' where id='"+id_update+"'");
					System.out.println(noOfRowsUpdated2+" rows updated");
				}
				catch(Exception e) 
				{
					e.printStackTrace();
				}

				break;
		   default:
				System.out.println("Sorry, but "+command+" is not a valid command. Please try again.");
				break;
			}

			break;
		}
	}

	
}