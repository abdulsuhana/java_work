package carinventoryproject;

public class Car_Details {
	String make;
	String model;
	int year;
	float sales_price;
	public Car_Details(String make, String model, int year, float sales_price)
	{
		this.make = make;
		this.model = model;
		this.year = year;
		this.sales_price = sales_price;
	}
	public String getMake()
	{
		return make;
	}
	public String getModel() 
	{
		return model;
	}
	public int getYear() 
	{
		return year;
	}
	public float getSales_price() 
	{
		return sales_price;
	}

	public String toString()
	{
		return  year+"  "+make+"  "+model+"  $"+sales_price;
	}

}
